<?php

function BILLALE_createFacture($order_id)
{
    $order = wc_get_order( $order_id );
    if(!$order){
        return "Order not Found";
    }
    $api = new BILLALE_api_alegra();

    $products = [] ;
    foreach ( $order->get_items() as $item_id => $item ) {
        $product_id = $item->get_product_id();
        $variation_id = $item->get_variation_id();
        $quantity = $item->get_quantity();

        if($variation_id !=0){
            $product_id = $variation_id;
        }
        $product = wc_get_product( $product_id );

        $BILLALE_product_alegra =  get_post_meta($product_id,"BILLALE_product_alegra",true); 
        if($BILLALE_product_alegra == null || $BILLALE_product_alegra == false || $BILLALE_product_alegra == ""){
            BILLALE_createProduct($product_id);
            $BILLALE_product_alegra =  get_post_meta($product_id,"BILLALE_product_alegra",true); 
        }

        $BILLALE_product_alegra = json_decode($BILLALE_product_alegra,true);

        if($BILLALE_product_alegra["id"] === null){
            
        }else{
            $products[] =  array(
                "id"        => $BILLALE_product_alegra["id"],
                "price"     => $product->get_price(),
                "quantity"  => $quantity
            );
        }

    }


    $client_id = BILLALE_getUserIdAlegra_byOrder($order);

    $result = $api->createFacture(array(
        "date"              =>  (new DateTime($order->get_date_paid()))->format('Y-m-d'),
        "dueDate"           =>  $order->get_date_paid()->format('Y-m-d'),
        "client"            =>  $client_id,
        "paymentForm"       =>  "CREDIT",
        "paymentMethod"     =>  $order->get_payment_method_title(),
        "items"             =>  $products
    ));

    update_post_meta($order_id,"BILLALE_facture_alegra",json_encode($result));
}