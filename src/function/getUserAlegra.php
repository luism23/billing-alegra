<?php

function BILLALE_getUserIdAlegra_byOrder($order)
{
    $email = $order->get_billing_email();
    $users = get_option("BILLALE_USERS");
    if($users === false || $users == null || $users == ""){
        $users = "{}";
    }
    $users = json_decode($users,true);

    $user = $users[$email];

    if($user["id"] != null){
        return $user["id"];
    }else{
        $api = new BILLALE_api_alegra();

        $result = $api->createContact(array(
            "name" => array(
                "firstName"=>$order->get_billing_first_name(),     
                "secondName"=>" ",
                 "lastName"=>$order->get_billing_last_name()
            ),
            "identificationObject" => array(
                "type"      => get_post_meta($order->get_id(),"billing_type_document",true),
                "number"    => get_post_meta($order->get_id(),"billing_document",true),
            ),
            "email"   => $email,
            "phonePrimary" => $order->get_billing_phone(),
            "type"    => array("client"),
            "kindOfPerson" => "PERSON_ENTITY",
            "address" => array(
                "address" => $order->get_billing_address_1(),
                "city" =>$order->get_billing_city()
            ),
        ));
        $users[$email] = $result;
        update_option("BILLALE_USERS",json_encode($users));
        return $result["id"];
    }
}