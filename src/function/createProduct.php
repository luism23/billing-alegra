<?php

function BILLALE_createProduct($product_id)
{
    $product = wc_get_product( $product_id );
    if(!$product){
        return "Product not Found";
    }
    $api = new BILLALE_api_alegra();

    $result = $api->createProduct(array(
        "name"  =>  $product->get_name(),
        "price" =>  $product->get_price(),
    ));

    update_post_meta($product_id,"BILLALE_product_alegra",json_encode($result));
}