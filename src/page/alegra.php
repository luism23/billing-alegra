<?php
function BILLALE_create_menu() {

	//create new top-level menu
	add_menu_page('Alegra Settings', 'Alegra', 'administrator', __FILE__, 'BILLALE_settings_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_BILLALE_settings' );
}
add_action('admin_menu', 'BILLALE_create_menu');


function register_BILLALE_settings() {
	//register our settings
	register_setting( 'alegra-settings-group', 'new_option_name' );
	register_setting( 'alegra-settings-group', 'some_other_option' );
	register_setting( 'alegra-settings-group', 'option_etc' );
}

function BILLALE_settings_page() {
    if($_POST){
        update_option("BILLALE_name",$_POST["BILLALE_name"]);
        update_option("BILLALE_token",$_POST["BILLALE_token"]);
    }
    ?>
    <div class="wrap">
        <h1>
            Alegra
        </h1>
        <form method="post">
            <label>
                <strong>User</strong>
                <br>
                <input type="text" name="BILLALE_name" id="BILLALE_name" value="<?=get_option("BILLALE_name")?>">
            </label>
            <br>
            <br>
            <label>
                <strong>Token</strong>
                <br>
                <input type="password" name="BILLALE_token" id="BILLALE_token" value="<?=get_option("BILLALE_token")?>">
            </label>
            <button class="button action" >
                Save
            </button>
        </form>
    </div>
    <?php 
}