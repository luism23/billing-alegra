<?php
if(BILLALE_LOG){
    function add_BILLALE_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'BILLALE_LOG',
                'title' => 'BILLALE_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=BILLALE_LOG'
            )
        );
    }

    function BILLALE_LOG_option_page()
    {
        add_options_page(
            'Log Alegra',
            'Log Alegra',
            'manage_options',
            'BILLALE_LOG',
            'BILLALE_LOG_page'
        );
    }

    function BILLALE_LOG_page()
    {
        $log = get_option("BILLALE_LOGS");
        if($log === false || $log == null || $log == ""){
            $log = "[]";
        }
        ?>
        <script>
            const log = <?=$log?>;
        </script>
        <h1>
            Solo se guardan las 100 peticiones
        </h1>
        <pre><?php var_dump(array_reverse(json_decode($log,true)));?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_BILLALE_LOG_option_page', 100);

    add_action('admin_menu', 'BILLALE_LOG_option_page');

}

function addBILLALE_LOG($newLog)
{
    if(!BILLALE_LOG){
        return;
    }
    $log = get_option("BILLALE_LOGS");
    if($log === false || $log == null || $log == ""){
        $log = "[]";
    }
    $log = json_decode($log);
    $log[] = $newLog;
    $log = array_slice($log, -100,100); 
    update_option("BILLALE_LOGS",json_encode($log));
}