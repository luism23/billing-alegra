<?php



class BILLALE_api_alegra
{
    private $URLAPI = "https://api.alegra.com/api/v1";

    private $TOKEN = "";
    private $USER = "";


    public function __construct()
    {
        $this->TOKEN = get_option("BILLALE_token");
        $this->USER = get_option("BILLALE_name");
    }
    private function request($url,$method,$json)
    {
        addBILLALE_LOG(array(
            "url"       =>  $url,
            "method"    =>  $method,
            "json"      =>  $json,
            "user"      =>  $this->USER,
            "token"     =>  $this->TOKEN,
            "base64_encode" => base64_encode($this->USER.":".$this->TOKEN),
        ));
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URLAPI.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS =>json_encode($json),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic '.base64_encode($this->USER.":".$this->TOKEN),
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        addBILLALE_LOG(array(
            "url"       =>  $url,
            "method"    =>  $method,
            "json"      =>  $json,
            "user"      =>  $this->USER,
            "token"     =>  $this->TOKEN,
            "base64_encode" => base64_encode($this->USER.":".$this->TOKEN),
            "respond"   => json_decode($response,true),
        ));
        return json_decode($response,true);

    }
    public function createProduct($data = array())
    {
        return $this->request(
            "/items",
            "POST",
            $data
        );
    }
    public function createFacture($data = array())
    {
        return $this->request(
            "/invoices",
            "POST",
            $data
        );
    }
    public function createContact($data = array())
    {
        return $this->request(
            "/contacts",
            "POST",
            $data
        );
    }
}
