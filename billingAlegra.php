<?php
/*
Plugin Name: Billing Alegra
Plugin URI: https://gitlab.com/luism23/billing-alegra
Description: plugin to create invoicing made by Luis Miguel
Author: Byte4bit
Version: 1.0.22
Author URI: https://gitlab.com/luism23/
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/billing-alegra',
	__FILE__,
	'billing-alegra'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');


define("BILLALE_LOG",false);
define("BILLALE_PATH",plugin_dir_path(__FILE__));
define("BILLALE_URL",plugin_dir_url(__FILE__));

require BILLALE_PATH."src/_index.php";